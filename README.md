Free source code for [Learn Meteor book](http://leanpub.com/learnmeteor) demos and do-it-yourself

# Get the book

Buy the book [on Amazon](https://www.amazon.com/Learn-Meteor-Node-js-JavaScript-platform-ebook/dp/B01J6K6SOG).

# Get the source code

Just `git clone` that repository. If that sounds obscure to you, click the "Downloads" link at the left of this window.