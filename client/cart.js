Template.cart.helpers({
    contents: function () {
        return Basket.find();
    }
});

Template.cart.events({
    'click .removeAction': function() {
        Basket.remove({_id: this._id});
    },
    'click .incrementAction': function() {
        Basket.update({_id: this._id},
        {$inc: {quantity: 1}});
    },
    'click .decrementAction': function() {
        Basket.update({
            _id: this._id,
            quantity: { $gt: 1 },
        },
        {$inc: {quantity: -1 }});
    }
});