Template.timeDisplay.helpers({
   currentTime: function() {
        var now = new Date();
        return now.toLocaleTimeString();
   } 
});

Template.news.helpers({
    news: function () {
        return News.find();
    }
});

Template.news.events({
    'submit form': function (e) {
        e.preventDefault();
        var title = e.target.title.value;
        Meteor.call('insertnews', title);
        //News.insert({title: title, created: new Date()});
    },
    'click .removeAction': function(e, a) {
        //News.remove({_id: this._id});
        Meteor.call('removenews', this._id);
    }
});
