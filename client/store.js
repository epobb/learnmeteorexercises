Basket = new Mongo.Collection(null);

Template.store.helpers({
    list: function () {
        return Products.find();
    }
});

Template.store.events({
    'click .buyAction': function () {
        Basket.insert({ product: this, quantity: 1});
    }
});