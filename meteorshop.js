News = new Mongo.Collection('news');
Products = new Mongo.Collection('products');

Products.attachSchema(new SimpleSchema({
    title: {
        type: String
    },
    price: {
        type: Number
    }
}));

if (Meteor.isClient) {
    Template.news.helpers({
        news: function () {
            return News.find();
        }
    });
}

