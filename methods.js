Meteor.methods({
    'removenews': function(id) {
        News.remove({_id: id});        
    },
    'insertnews': function(title) {
        News.insert({
            title: title,
            created: new Date()
        });
    }
});