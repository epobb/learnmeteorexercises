Router.configure({
  layoutTemplate: 'layout'
}); 

Router.route('/', function () {
  this.render('home');
});

Router.route('/cart', function () {
  this.render('cart');
});

Router.route('/products', function () {
  this.render('products');
});

Router.route('/time', function () {
  this.render('timeDisplay');
});

AccountsTemplates.configure({
  defaultLayout: 'layout',
});

Router.plugin('ensureSignedIn', {
  only: ['products']
});